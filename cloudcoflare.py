import CloudFlare
import creds
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(419, 238)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.txtName = QtWidgets.QLineEdit(self.centralwidget)
        self.txtName.setObjectName("txtName")
        self.gridLayout.addWidget(self.txtName, 6, 1, 1, 1)
        self.txtTarget = QtWidgets.QLineEdit(self.centralwidget)
        self.txtTarget.setObjectName("txtTarget")
        self.gridLayout.addWidget(self.txtTarget, 6, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 7, 0, 1, 3)
        self.btnSubmit = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Comic Sans MS")
        font.setPointSize(10)
        self.btnSubmit.setFont(font)
        self.btnSubmit.setObjectName("btnSubmit")
        self.gridLayout.addWidget(self.btnSubmit, 8, 0, 1, 3)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 3, 0, 1, 3)
        self.lblType = QtWidgets.QLabel(self.centralwidget)
        self.lblType.setObjectName("lblType")
        self.gridLayout.addWidget(self.lblType, 5, 0, 1, 1)
        self.lblName = QtWidgets.QLabel(self.centralwidget)
        self.lblName.setObjectName("lblName")
        self.gridLayout.addWidget(self.lblName, 5, 1, 1, 1)
        self.lblHeader = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.lblHeader.setFont(font)
        self.lblHeader.setObjectName("lblHeader")
        self.gridLayout.addWidget(self.lblHeader, 0, 0, 1, 3, QtCore.Qt.AlignHCenter)
        self.lblTarget = QtWidgets.QLabel(self.centralwidget)
        self.lblTarget.setObjectName("lblTarget")
        self.gridLayout.addWidget(self.lblTarget, 5, 2, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 2, 0, 1, 3)
        self.cbRecType = QtWidgets.QComboBox(self.centralwidget)
        self.cbRecType.setObjectName("cbRecType")
        self.cbRecType.addItem("")
        self.cbRecType.addItem("")
        self.gridLayout.addWidget(self.cbRecType, 6, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 4, 0, 1, 3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 419, 18))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # actions
        self.btnSubmit.clicked.connect(self.sendDnsRecord)
        self.cbRecType.activated.connect(self.updateLabel)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "CloudCoFlare"))
        self.txtName.setPlaceholderText(_translate("MainWindow", "yse01customerdomain01"))
        self.txtTarget.setPlaceholderText(_translate("MainWindow", "23.90.82.73"))
        self.btnSubmit.setText(_translate("MainWindow", "SCHMING!"))
        self.lblType.setText(_translate("MainWindow", "Type"))
        self.lblName.setText(_translate("MainWindow", "Name"))
        self.lblHeader.setText(_translate("MainWindow", "CloudFlare DNS Record Creator / Updater"))
        self.lblTarget.setText(_translate("MainWindow", "IPv4 Address"))
        self.cbRecType.setItemText(0, _translate("MainWindow", "A"))
        self.cbRecType.setItemText(1, _translate("MainWindow", "CNAME"))

    
    def sendDnsRecord(self):
        zone_name = 'cloudcopartner.com'
        cf = CloudFlare.CloudFlare(email=creds.creds['admin_email'], token=creds.creds['apikey'])

        # query the zone name and expect only one value back
        try:
            zones = cf.zones.get(params={'name': zone_name, 'per_page': 1})
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones.get %d %s - api call failed' % (e, e))
        except Exception as e:
            exit('/zones.get - %s api call failed' % e)

        if len(zones) == 0:
            exit('No  zones found')

        # extract the zone_id which is needed to process that zone
        zone = zones[0]
        zone_id = zone['id']

        # request the DNS records from that zone
        try:
            dns_records = cf.zones.dns_records.get(zone_id)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones/dns_records.get %d %s - api call failed' % (e, e))

        dns_record = {
            'type': self.cbRecType.currentText(),
            'name': self.txtName.text(),
            'content': self.txtTarget.text(),
            'proxied': False
        }

        try:
            r = cf.zones.dns_records.post(zone_id, data=dns_record)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            # exit('/zones.dns_records.post %s %s - %d %s' % (zone_name, dns_record['name'], e, e))
            msgFail = QMessageBox()
            msgFail.setWindowTitle('Aw, schming!')
            msgFail.setText('Something got schminged up!')
            msgFail.setInformativeText(f'Error: {e}')
            msgFail.setIcon(QMessageBox.Critical)

        try:
            dns_record = r

            print('\t%s %30s %6d %-5s %s ; proxied=%s proxiable=%s' % (
            dns_record['id'],
            dns_record['name'],
            dns_record['ttl'],
            dns_record['type'],
            dns_record['content'],
            dns_record['proxied'],
            dns_record['proxiable']
            ))

            msgSuccess = QMessageBox()
            msgSuccess.setWindowTitle('Great Success!')
            msgSuccess.setText(f'Set successful {dns_record["type"]} record!')
            msgSuccess.setInformativeText(f'Name: {dns_record["name"]}\nContent: {dns_record["content"]}')
            msgSuccess.setIcon(QMessageBox.Information)
            msgSuccess.exec_()
        except UnboundLocalError as e2:
            msgFail = QMessageBox()
            msgFail.setWindowTitle('Aw, schming!')
            msgFail.setText('Something got schminged up!')
            msgFail.setInformativeText(f'You probably left something blank\nError: {e2}')
            msgFail.setIcon(QMessageBox.Critical)
            msgFail.exec_()

        

    def updateLabel(self):
        if self.cbRecType.currentText() == 'A':
            self.lblTarget.setText('IPv4 Address')
            self.txtTarget.setPlaceholderText('23.90.82.73')
        else:
            self.lblTarget.setText('Target')
            self.txtTarget.setPlaceholderText('yse01customerdomain01')


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
