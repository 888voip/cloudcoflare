import CloudFlare
import creds

zone_name = 'cloudcopartner.com'
cf = CloudFlare.CloudFlare(email=creds.creds['admin_email'], token=creds.creds['apikey'])

# query the zone name and expect only one value back
try:
    zones = cf.zones.get(params={'name': zone_name, 'per_page': 1})
except CloudFlare.exceptions.CloudFlareAPIError as e:
    exit('/zones.get %d %s - api call failed' % (e, e))
except Exception as e:
    exit('/zones.get - %s api call failed' % e)

if len(zones) == 0:
    exit('No  zones found')

# extract the zone_id which is needed to process that zone
zone = zones[0]
zone_id = zone['id']

# request the DNS records from that zone
try:
    dns_records = cf.zones.dns_records.get(zone_id)
except CloudFlare.exceptions.CloudFlareAPIError as e:
    exit('/zones/dns_records.get %d %s - api call failed' % (e, e))

record_name = input("What is the name of the record (subdomain only, do no include '.cloudcopartner.com')? ")
record_type = input("What type of record (A, AAAA, TXT)? ")
record_content = input("What is the content of the record (ip address)? ")
if record_type == '':
    record_type = 'A'

dns_record = {
    'type': record_type,
    'name': record_name,
    'content': record_content,
    'proxied': False
}

try:
    r = cf.zones.dns_records.post(zone_id, data=dns_record)
except CloudFlare.exceptions.CloudFlareAPIError as e:
    exit('/zones.dns_records.post %s %s - %d %s' % (zone_name, dns_record['name'], e, e))

dns_record = r
print('\t%s %30s %6d %-5s %s ; proxied=%s proxiable=%s' % (
    dns_record['id'],
    dns_record['name'],
    dns_record['ttl'],
    dns_record['type'],
    dns_record['content'],
    dns_record['proxied'],
    dns_record['proxiable']
))
